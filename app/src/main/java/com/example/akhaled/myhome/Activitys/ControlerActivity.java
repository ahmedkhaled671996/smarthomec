package com.example.akhaled.myhome.Activitys;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.example.akhaled.myhome.FloatingWindow;
import com.example.akhaled.myhome.R;
import com.example.akhaled.myhome.fragments.ControlerFragment;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import dmax.dialog.SpotsDialog;

public class ControlerActivity extends AppCompatActivity {

    View  parentLayout;
    Activity activity ;
    BluetoothDevice bluetoothDevice;
    AlertDialog alertDialog;

    Button startButton;

    Bundle bundle;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controler);

        bluetoothDevice = getIntent().getBundleExtra("mybundle").getParcelable("myBT");

        activity = this;
        parentLayout = findViewById(android.R.id.content);
        startButton = findViewById(R.id.start_fw);
        alertDialog = new SpotsDialog(this,R.style.Custom);

        bundle = new Bundle();
        bundle.putParcelable("mohamed", bluetoothDevice);
        intent = new Intent(ControlerActivity.this, FloatingWindow.class)
                .putExtra("fuck", bundle);



        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (startButton.getText().equals("Start")) {

                    startService(intent);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    startButton.setText(R.string.stop);

                }else {

                    stopService(intent);
                    startButton.setText(R.string.start);

                }

            }
        });

//        startNotification();

//        Snackbar.make(parentLayout, "try connect", Snackbar.LENGTH_SHORT)
//                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
//                .show();

//        if (BTconnect(bluetoothDevice)) {
//            Snackbar.make(parentLayout, "succffly connected", Snackbar.LENGTH_SHORT)
//                    .show();
//        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(intent);
    }

    class MyThread extends Thread {


        @Override
        public void run() {


            try {
                Thread.sleep(2000);
            }catch (Exception e){

            }

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    alertDialog.dismiss();
                }
            });

        }

    }

}

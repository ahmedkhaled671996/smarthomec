package com.example.akhaled.myhome.Activitys;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.akhaled.myhome.R;
import com.example.akhaled.myhome.fragments.ControlerFragment;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import dmax.dialog.SpotsDialog;

public class MainActivity extends AppCompatActivity {



    SharedPreferences sharedPreferences;
    BluetoothAdapter bluetoothAdapter;
    ProgressBar progressBar ;

    View parentLayout ;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //        alertDialog = new SpotsDialog(this,R.style.Custom);
        progressBar = findViewById(R.id.progress_Bar);

        sharedPreferences = this.getSharedPreferences("BTMAC", Context.MODE_PRIVATE);
        parentLayout = findViewById(android.R.id.content);


    }


    @Override
    protected void onResume() {
        super.onResume();
        MyThread myThread = new MyThread() ;
        myThread.start();

    }

    class MyThread extends Thread {


        @Override
        public void run() {

            final boolean[] rebatr = {true,true};

            while (rebatr[0]) {

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();

                        if (bluetoothAdapter == null) //Checks if the device supports bluetooth
                        {

                            Snackbar.make(parentLayout, "Device doesn't support bluetooth", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("CLOSE", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finish();
                                        }
                                    })
                                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                    .show();
                            rebatr[0] = false;

                        }
                        //Checks if bluetooth is enabled. If not, the program will ask permission from the user to enable it
                        else if ((!bluetoothAdapter.isEnabled())&&rebatr[1]) {

                            Snackbar.make(parentLayout, "bluetooth is not Enabled ", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("Enabl", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent enableAdapter = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                                            startActivityForResult(enableAdapter, 0);
                                        }
                                    })
                                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                    .show();
                            rebatr[1] = false;
                        }
                        else if (bondedDevices.isEmpty()&&bluetoothAdapter.isEnabled()) {
                            Snackbar.make(parentLayout, "Please pair the device first", Snackbar.LENGTH_INDEFINITE)
                                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                    .show();
                            rebatr[0] = false;

                        } else if (bluetoothAdapter.isEnabled()) {
                            rebatr[0] = false;

                            for (BluetoothDevice iterator : bondedDevices) {
                                if (iterator.getAddress().equals(sharedPreferences.getString("DMAC", "null"))) {

                                    Snackbar.make(parentLayout, "Devise foind...", Snackbar.LENGTH_LONG)
                                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                            .show();

                                    progressBar.setVisibility(View.GONE);

                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable("myBT",iterator);

                                    Intent intent = new Intent(activity,ControlerActivity.class);
                                    intent.putExtra("mybundle",bundle);
                                    startActivity(intent);
                                    break;

                                }else {
                                    Snackbar.make(parentLayout, "Devise not foind", Snackbar.LENGTH_SHORT)
                                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                            .show();

                                    progressBar.setVisibility(View.GONE);
                                    getSupportFragmentManager()
                                            .beginTransaction()
                                            .add(R.id.fragment_content , new com.example.akhaled.myhome.fragments.ListFragment())
                                            .commit();
                                    break;
                                }

                            }

                        }

                    }
                });

                try {
                    Thread.sleep(2000);
                }catch (Exception e){

                }

            }

        }

    }
}

/**try {

 if (BTconnect(iterator)){
 Snackbar.make(parentLayout, "saccssfly connected", Snackbar.LENGTH_LONG)
 .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
 .show();
 progressBar.setVisibility(View.GONE);
 constraintLayout.setVisibility(View.VISIBLE);
 start = true;
 break;
 }
 }
 catch(Exception e) {
 e.printStackTrace();
 Snackbar.make(parentLayout, "feild connected ", Snackbar.LENGTH_INDEFINITE)
 .setAction("try agean", new View.OnClickListener() {
@Override
public void onClick(View view) {
MyThread myThread = new MyThread() ;
myThread.start();
}
})
 .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
 .show();
 break;
 }

 Button button = findViewById(R.id.start);
 button.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

if (start){
}else {

Snackbar.make(parentLayout, "feild connected ", Snackbar.LENGTH_INDEFINITE)
.setAction("try agean", new View.OnClickListener() {
@Override
public void onClick(View view) {
MyThread myThread = new MyThread() ;
myThread.start();
}
})
.setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
.show();

}

}
});



 **/
package com.example.akhaled.myhome;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

public class FloatingWindow extends Service {

    public WindowManager wm;
    LinearLayout ll , linearLayout;
    LayoutParams parameters;
    BluetoothDevice bluetoothDevice;

    private BluetoothSocket socket;
    private OutputStream outputStream;

    String command1 = "c" , command2 = "a" , command3 = "e" ,command4 = "g";

    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressLint({"NewApi", "ClickableViewAccessibility"})
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        bluetoothDevice = intent.getBundleExtra("fuck").getParcelable("mohamed");

        if (BTconnect(bluetoothDevice)){
            Toast.makeText(FloatingWindow.this, "connected", Toast.LENGTH_SHORT).show();
        }

        wm = (WindowManager) getSystemService(WINDOW_SERVICE);

        ll = new LinearLayout(this);
        ll.setBackground(getDrawable(R.drawable.ic_all_out_black_24dp));
        LinearLayout.LayoutParams layoutParameteres = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ll.setLayoutParams(layoutParameteres);


        linearLayout = new LinearLayout(this);//i can not do that
        linearLayout.setBackgroundColor(Color.GRAY);
        LinearLayout.LayoutParams layoutParameteres2 = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        ll.setBackgroundColor(Color.argb(66,255,0,0));
        linearLayout.setLayoutParams(layoutParameteres2);

        parameters = new LayoutParams(
                150, 150, LayoutParams.TYPE_PHONE,
                LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        parameters.gravity = Gravity.CENTER | Gravity.CENTER;
        parameters.x = 0;
        parameters.y = 0;

        final Button b1 = new Button(this);
        final Button b2 = new Button(this);
        final Button b3 = new Button(this);
        final Button b4 = new Button(this);
        final Button back = new Button(this);

        ViewGroup.LayoutParams btnParameters1 = new ViewGroup.LayoutParams(120, ViewGroup.LayoutParams.MATCH_PARENT);

        b1.setText("B1");
        b1.setLayoutParams(btnParameters1);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (socket.isConnected()) {

                    if (command1.equals("c")) {
                        command1 = "d";
                        try {
                            outputStream.write(command1.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        command1 = "c";
                        try {
                            outputStream.write(command1.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    Toast.makeText(FloatingWindow.this, "connection error", Toast.LENGTH_SHORT).show();
                }
            }
        });

        b2.setText("B2");
        b2.setLayoutParams(btnParameters1);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (socket.isConnected()) {

                    if (command2.equals("a")) {
                        command2 = "b";
                        try {
                            outputStream.write(command2.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        command2 = "a";
                        try {
                            outputStream.write(command2.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    Toast.makeText(FloatingWindow.this, "connection error", Toast.LENGTH_SHORT).show();
                }            }
        });

        b3.setText("B3");
        b3.setLayoutParams(btnParameters1);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (socket.isConnected()) {

                    if (command3.equals("e")) {
                        command3 = "f";
                        try {
                            outputStream.write(command3.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        command3 = "e";
                        try {
                            outputStream.write(command3.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    Toast.makeText(FloatingWindow.this, "connection error", Toast.LENGTH_SHORT).show();
                }
            }
        });

        b4.setText("B4");
        b4.setLayoutParams(btnParameters1);
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (socket.isConnected()) {

                    if (command4.equals("g")) {
                        command4 = "h";
                        try {
                            outputStream.write(command4.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        command4 = "g";
                        try {
                            outputStream.write(command4.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    Toast.makeText(FloatingWindow.this, "connection error", Toast.LENGTH_SHORT).show();
                }
            }
        });

        back.setText("X");
        back.setBackgroundColor(Color.RED);
        back.setLayoutParams(btnParameters1);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wm.removeView(linearLayout);
                wm.addView(ll,parameters);
            }
        });

        linearLayout.addView(b1);
        linearLayout.addView(b2);
        linearLayout.addView(b3);
        linearLayout.addView(b4);
        linearLayout.addView(back);

        wm.addView(ll, parameters);
        ll.setOnTouchListener(new View.OnTouchListener() {
            LayoutParams updatedParameters = parameters;
            double x;
            double y;
            double pressedX;
            double pressedY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        x = updatedParameters.x;
                        y = updatedParameters.y;

                        pressedX = event.getRawX();
                        pressedY = event.getRawY();

                        break;

                    case MotionEvent.ACTION_MOVE:
                        updatedParameters.x = (int) (x + (event.getRawX() - pressedX));
                        updatedParameters.y = (int) (y + (event.getRawY() - pressedY));

                        wm.updateViewLayout(ll, updatedParameters);

                    default:
                        break;
                }

                return false;
            }
        });

        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutParams parameters2 = new LayoutParams(
                        600, 200, LayoutParams.TYPE_PHONE,
                        LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
                parameters2.gravity = Gravity.CENTER | Gravity.CENTER;
                parameters2.x = 0;
                parameters2.y = 0;
                wm.removeView(ll);
                wm.addView(linearLayout,parameters2);

            }
        });



        return super.onStartCommand(intent, flags, startId);
    }

    @SuppressLint({"ClickableViewAccessibility", "NewApi"})
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        Toast.makeText(FloatingWindow.this, "try connecting", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        stopSelf();
        System.exit(0);
    }

    public boolean BTconnect(BluetoothDevice bluetoothDevice) {


        boolean connected = true;

        try {
            socket = bluetoothDevice.createRfcommSocketToServiceRecord(PORT_UUID); //Creates a socket to handle the outgoing connection
            socket.connect();
        }
        catch(IOException e) {
            e.printStackTrace();
            connected = false;
        }

        if(connected)
        {
            try {
                outputStream = socket.getOutputStream(); //gets the output stream of the socket
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }

        return connected;
    }

    class MyThread extends Thread {


        @Override
        public void run() {


            try {
                Thread.sleep(5000);
            }catch (Exception e){

            }

            Toast.makeText(FloatingWindow.this, "thread", Toast.LENGTH_SHORT).show();

        }

    }


}
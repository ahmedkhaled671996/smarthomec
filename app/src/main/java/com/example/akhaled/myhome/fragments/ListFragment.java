package com.example.akhaled.myhome.fragments;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.akhaled.myhome.Activitys.ControlerActivity;
import com.example.akhaled.myhome.Activitys.MainActivity;
import com.example.akhaled.myhome.R;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class ListFragment extends Fragment {


    View view;
    ArrayList<String> names;
    ArrayList<BluetoothDevice> devices;
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.list_fragment,container,false);

        listView = view.findViewById(R.id.BluetoothList);

        names = new ArrayList<>();
        devices = new ArrayList<>();

        if(BTinit()){
            ArrayAdapter arrayAdapter = new ArrayAdapter(
                    getContext()
                    ,android.R.layout.simple_list_item_1
                    ,names);

            listView.setAdapter(arrayAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("BTMAC", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.putString("DMAC",devices.get(position).getAddress());
                    editor.apply();


                    Bundle bundle = new Bundle();
                    bundle.putParcelable("myBT",devices.get(position));

                    Intent intent = new Intent(getActivity(),ControlerActivity.class);
                    intent.putExtra("mybundle",bundle);
                    startActivity(intent);

                }
            });
        }
        return view;

    }

    public boolean BTinit() {

        boolean found = false;

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();

        if(!bondedDevices.isEmpty()) //Checks for paired bluetooth devices
        {
            names.clear();
            devices.clear();
            for(BluetoothDevice iterator : bondedDevices) {

                names.add(iterator.getName());
                devices.add(iterator);

            }
            found = true;
        }

        return found;
    }


}
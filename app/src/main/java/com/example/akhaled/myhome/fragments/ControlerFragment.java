package com.example.akhaled.myhome.fragments;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.akhaled.myhome.R;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

public class ControlerFragment  extends Fragment {


    View view , parentLayout;

    private BluetoothSocket socket;
    private OutputStream outputStream;

    String command1 = "" , command2 = "" , command3 = "" ,command4 = "";

    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    Button b1 , b2 ,b3 , b4;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.controler_fragment,container,false);
        parentLayout = getActivity().findViewById(android.R.id.content);
//
//        b1 = view.findViewById(R.id.b1);
//        b2 = view.findViewById(R.id.b2);
//        b3 = view.findViewById(R.id.b3);
//        b4 = view.findViewById(R.id.b4);


        return view;

    }

    @Override
    public void onResume() {
        super.onResume();

        BluetoothDevice bluetoothDevice = this.getArguments().getParcelable("myBT");

        Snackbar.make(parentLayout, "try connect", Snackbar.LENGTH_SHORT)
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();

        if (BTconnect(bluetoothDevice)) {
            Snackbar.make(parentLayout, "succffly connected", Snackbar.LENGTH_SHORT)
                    .show();

        }

//        startControul();

        MyThread thread = new MyThread();
        thread.start();

    }

    public boolean BTconnect(BluetoothDevice bluetoothDevice) {


        boolean connected = true;

        try {
            socket = bluetoothDevice.createRfcommSocketToServiceRecord(PORT_UUID); //Creates a socket to handle the outgoing connection
            socket.connect();
        }
        catch(IOException e) {
            e.printStackTrace();
            connected = false;
        }

        if(connected)
        {
            try {
                outputStream = socket.getOutputStream(); //gets the output stream of the socket
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }

        return connected;
    }

//    public void startControul(){
//
//
//        b1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (socket.isConnected()) {
//
//
//                    if (command1.equals("c")) {
//                        command1 = "d";
//                        try {
//                            outputStream.write(command1.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        command1 = "c";
//                        try {
//                            outputStream.write(command1.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }else {
//                    Snackbar.make(parentLayout, "connection error", Snackbar.LENGTH_SHORT)
//                            .show();
//                }
//            }
//        });
//
//        b2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (socket.isConnected()) {
//
//
//                    if (command1.equals("a")) {
//                        command1 = "b";
//                        try {
//                            outputStream.write(command1.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        command1 = "a";
//                        try {
//                            outputStream.write(command1.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }else {
//                    Snackbar.make(parentLayout, "connection error", Snackbar.LENGTH_SHORT)
//                            .show();
//                }
//            }
//        });
//
//        b3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (socket.isConnected()) {
//
//                    if (command1.equals("e")) {
//                        command1 = "f";
//                        try {
//                            outputStream.write(command1.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        command1 = "e";
//                        try {
//                            outputStream.write(command1.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }else {
//                    Snackbar.make(parentLayout, "connection error", Snackbar.LENGTH_SHORT)
//                            .show();
//                }
//            }
//        });
//
//        b4.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (socket.isConnected()) {
//
//                    if (command1.equals("g")) {
//                        command1 = "h";
//                        try {
//                            outputStream.write(command1.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        command1 = "g";
//                        try {
//                            outputStream.write(command1.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }else {
//                    Snackbar.make(parentLayout, "connection error", Snackbar.LENGTH_SHORT)
//                            .show();
//                }
//            }
//        });
//    }

    public void createNotifaciton(){

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){

            CharSequence charSequence = "my noti";

        }

    }

    class MyThread extends Thread {


        @Override
        public void run() {

                try {
                    Thread.sleep(3000);
                }catch (Exception e){

                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!socket.isConnected()){
                            Snackbar.make(parentLayout, "feald connation... ", Snackbar.LENGTH_SHORT)
                                    .setAction("retray", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getActivity().recreate();
                                        }
                                    })
                                    .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                                    .show();
                        }
                    }
                });

         }

      }


}


